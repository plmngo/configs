[COMMANDS]
; Enables or Disables the Player Command .help
; Note 1 : Shows the content of PlayerHelp.txt file for the player.
HELP=false
; Enables or Disables the Player Command .online
; Note 1 : Shows the currently amount of online players.
ONLINE=true
; Enables or Disables the Player Command .time
; Note 1 : Shows currently server time
TIME=true
; Enables or Disables the Player Command .expon and .expoff
; Note 1 : Turns on/off the exp gaining when killing mobs, etc.
EXP=false
; Enables or Disables the VIP Player Command .offlineshop
; Note 1 : The player must have privilege set in user_vip table to use this command !
OFFLINESHOP=false
; Enables or Disables the VIP Player Command .autopickupon and .autopickupoff
; Note 1 : The player must have privilege set in user_vip table to use this command !
AUTOPICKUP=true
; Enables or Disables the Offline Shop Announcement Command !
; Note 1 : The maximum allowed characters are 128 !
; Note 2 : You can adjust the spam timeout in OfflineShop.ini !
OFFANNOUNCE=false
; Allow .menu command that show <MENU_HTML> to players
; bypass -h _allblock?state=1|0
; bypass -h _online
; bypass -h _time
; bypass -h _expon
; bypass -h _expoff
; bypass -h _autopickupon
; bypass -h _autopickupoff
; bypass -h _menu
; bypass -h _lock
; bypass -h _unlock?password=
; bypass -h _setlockpw?password=
MENU=false
MENU_HTML=menu.htm
AUTOPICKUP_FOR_ALL=false